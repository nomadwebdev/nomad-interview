const db = require('./database');
const Customer = require('./database/customer');
const User = require('./database/user');


/*

  We have a collection of customer profile documents which contains duplicates.

  We need to group these duplicate profiles based on the license number field (case insensitive) to create a new "user" document.

  The use document should contain single values for each field as follows
    registrationDate: value of the cutsomer profile with the oldest "registrationDate" field
    lastVisitDate: value of the cutsomer profile with the newest "lastVisitDate" field
    name: value of the cutsomer profile with the newest "registrationDate" field
    emailEnabled: true if any profile has a emailEnabled of true
    loyaltyPoints: a sum of all loyaltyPoints for each profile
    customerProfiles: an array of the customer profile "ids" used to create the userSchema
    licenseNumber: the uppercase version of the licenseNumber used to group the profile

  Insert the new user documents into the user collection.

*/

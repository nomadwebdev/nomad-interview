//Connect to Mongo database
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const uri = `mongodb+srv://nomad:nomadinterview@nomad-interview.grgxd.mongodb.net/interview?retryWrites=true&w=majority`


mongoose.connect(uri, { useNewUrlParser: true, useFindAndModify: false}).then(
    () => {
        /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
        console.log('Connected to Mongo');
    },
    err => {
         /** handle initial connection error */
         console.log('error connecting to Mongo: ')
         console.log(err);
        }
  );


module.exports = mongoose.connection

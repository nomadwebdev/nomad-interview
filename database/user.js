const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  licenseNumber: String,
  name: String,
  emailEnabled: Boolean,
  registrationDate: Date,
  lastVisitDate: Date,
  loyaltyPoints: {
    type: Number,
    default: 0
  },
  customerProfiles: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer'
  }]
});

var User = mongoose.model('User', userSchema);
module.exports = User;

const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
  licenseNumber: String,
  name: String,
  emailEnabled: Boolean,
  registrationDate: Date,
  lastVisitDate: Date,
  loyaltyPoints: {
    type: Number,
    default: 0
  }
});

var Customer = mongoose.model('Customer', customerSchema);
module.exports = Customer;
